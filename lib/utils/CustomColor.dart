
import 'dart:ui';

class CustomColor{
  static const Color dark_gray =  Color(0xE91C1B1B);
  static const Color primaryColor =  Color(0xFFDD3944);
  static const Color main_light =  Color(0x3B0086B3);
  static const Color edit_color =  Color(0xFFE5E5E5);
  static const Color checkin_background_color = Color(0xffC4C4C4);
  static const Color checkout_background_color = Color(0xffD87E64);
  static const Color stepin_out_background_color = Color(0xffd2e4eb);
  static const Color selected_background_color = Color(0xff29B868);
  static const Color my_task_color = Color(0xfffdebec);
  static const Color secondryColor = Color(0xffea8879);
  static const Color iconBackgroundColor = Color(0xffffeeef);
  static const Color green_Accent = Color(0xff67cc94);
  static const Color light_gray = Color(0xffc4c4c4);
  static const Color primary_light = Color(0x1bdd3944);
}