import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/state_manager.dart';
import 'package:getxlearning/ui/auth/login.dart';
import 'package:getxlearning/ui/movies/home_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      debugShowCheckedModeBanner: false,
      home: const LoginPage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;
  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final controller = Get.put(Controller());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'You have pushed the button this many times:',
            ),

            Obx(() => Text(
                  'clicks: ${controller.count}',
                )),

            ElevatedButton(
              child: const Text('Navigate to Next Page'),
              onPressed: () {
                Get.to(() => NextPage());
              },
            ),
          ],
        ),
      ),

      floatingActionButton: FloatingActionButton(
        onPressed: controller.increment,
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}


class Controller extends GetxController {
  var count = 0.obs;
  void increment() {
    count++;
    update();
  }
}

class NextPage extends StatelessWidget {
  final Controller ctrl = Get.find();
  @override
  Widget build(context){
    return Scaffold(
      appBar: AppBar(
        title: const Text("Next Page"),
      ),
        body: Center(child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Text(
              'This is second page there showing the count value:',
            ),

            Text("${ctrl.count}"),
            ElevatedButton(
                onPressed: () {
                  Get.bottomSheet(
                      Wrap(
                        children: [
                          ListTile(
                            leading: Icon(Icons.wb_sunny_outlined),
                            title: Text("Light theme"),
                            onTap: () => {Get.changeTheme(ThemeData.light())},
                          ),
                          ListTile(
                            leading: Icon(Icons.wb_sunny),
                            title: Text("Dark theme"),
                            onTap: () => {Get.changeTheme(ThemeData.dark())},
                          )
                        ],
                      ),
                      backgroundColor: Colors.green
                  );
                },
                child: const Text('Show BottomSheet')),
            ElevatedButton(
              child: const Text('Back to Page'),
              onPressed: () {
                Get.back();
              },
            ),
          ],
        )));
  }
}