import 'package:http/http.dart' as http;

import '../movies/modal/casts.dart';
import '../movies/modal/upcomings.dart';


class MovieDBAPI {
  static var client = http.Client();
  static var apiKey = "a92f28e11a27e8e5938a2020be68ba9c";

  static Future<List<Movie>> fetchUpComingMovies() async {
    var response = await client
        .get(Uri.parse('https://api.themoviedb.org/3/movie/upcoming?api_key=$apiKey'));
    if (response.statusCode == 200) {
      var json = response.body;
      var upcoming = upcomingFromJson(json);
      return upcoming.results;
    } else {
      var json = response.body;
      var upcoming = upcomingFromJson(json);
      return upcoming.results;
    }
  }

  static Future<List<Cast>> fetchCastOfMovie({required int movieID}) async {
    var response = await client.get(
        Uri.parse('https://api.themoviedb.org/3/movie/$movieID/credits?api_key=$apiKey'));
    if (response.statusCode == 200) {
      var json = response.body;
      var castsResp = castsFromJson(json);
      return castsResp.cast;
    } else {
      var json = response.body;
      var castsResp = castsFromJson(json);
      return castsResp.cast;
    }
  }
}