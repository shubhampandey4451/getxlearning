import 'package:flutter/material.dart';

import 'castList.dart';
import 'modal/upcomings.dart';


class DetailPage extends StatelessWidget {
  Movie detail;
  DetailPage(this.detail, {super.key});

  Widget build(BuildContext context) {
    var casts =  CastList(detail.id);

    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.blue,
          title: const Text("Movie Details"),
        ),
        body: SingleChildScrollView(
            child:Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Image.network(
                  "https://image.tmdb.org/t/p/w185_and_h278_bestv2/${detail.posterPath}",
                  height: 500, width: MediaQuery.of(context).size.width,
                  fit: BoxFit.fill,
                ),
                Padding(padding: EdgeInsets.all(10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("ID : ${detail.id}"),
                      Text("Title : ${detail.title}"),
                      Text("Popularity :${detail.popularity}")
                    ],
                  ),),
                casts
              ],
            ))
    );
  }
}