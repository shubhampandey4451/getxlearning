import 'package:get/state_manager.dart';

import '../../network/api.dart';
import '../modal/upcomings.dart';


class UpComingController extends GetxController {
  var isLoading = true.obs;
  var movies = <Movie>[].obs;

  void fetchUpComing() async {
    try {
      isLoading(true);
      var res = await MovieDBAPI.fetchUpComingMovies();
      movies.assignAll(res);
    } finally {
      isLoading(false);
    }
  }
}