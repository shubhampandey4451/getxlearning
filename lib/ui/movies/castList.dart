import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/get_state_manager.dart';

import 'controller/castsController.dart';

class CastList extends StatelessWidget {
  int movieID;

  CastList(this.movieID, {super.key});

  final CastController castController = Get.put(CastController());

  @override
  Widget build(BuildContext context) {
    castController.fetchCasts(movieID: movieID);

    return SizedBox(
        height: 250,
        child: Obx(() {
          if (castController.isLoading.value) {
            return const Center(child: CircularProgressIndicator());
          } else {
            return GridView.count(
              childAspectRatio: 2,
              crossAxisCount: 1,
              scrollDirection: Axis.horizontal,
              children: List.generate(castController.casts.length, (index) {
                return Container(
                  margin: const EdgeInsets.only(top: 10,left: 10, right: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Image.network(
                          "https://image.tmdb.org/t/p/w185_and_h278_bestv2${castController.casts[index].profilePath}",
                          height: 150),
                      const SizedBox(height: 10,),
                      Text(castController.casts[index].name??"")
                    ],
                  ),
                );
              }),
            );
          }
        }));
  }
}