import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'controller/upcomingMovieController.dart';
import 'details_page.dart';


class HomePage extends StatelessWidget {
  final UpComingController upcomingController = Get.put(UpComingController());

  @override
  Widget build(BuildContext context) {
    upcomingController.fetchUpComing();
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue,
        title: const Text("Movie List"),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Expanded(child: Obx(() {
            if (upcomingController.isLoading.value) {
              return const Center(child: CircularProgressIndicator());
            } else {
              return Padding(padding: EdgeInsets.all(10),
                  child:GridView.count(
                      crossAxisCount: 2,
                      mainAxisSpacing: 10,
                      crossAxisSpacing: 10,
                      childAspectRatio: 1,
                      children:
                      List.generate(upcomingController.movies.length, (index) {
                        return Center(
                            child: GestureDetector(
                                onTap: () {
                                  Get.to(() => DetailPage(upcomingController.movies[index]));
                                },
                                child: Image.network(
                                  "https://image.tmdb.org/t/p/w185_and_h278_bestv2/${upcomingController
                                      .movies[index].posterPath}",
                                  height: 200,
                                  width: MediaQuery.of(context).size.width,
                                  fit: BoxFit.fill,
                                ),));
                      }))
              );
            }
          }))
        ],
      ),
    );
  }
}