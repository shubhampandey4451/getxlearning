

import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AuthController extends GetxController {
  final GlobalKey<FormState> signinForm = GlobalKey<FormState>();
  late TextEditingController emailController;
  late TextEditingController forgot_emailController;
  late TextEditingController passwordController;
  late TextEditingController old_passwordController;
  late TextEditingController confirmPasswordController;

  late TextEditingController phoneNumbberController;
  late TextEditingController nameController;
  late TextEditingController designationController;
  late TextEditingController reportingManagerController;
  late TextEditingController dateOfjoinngController;
  late TextEditingController emplyoeeIdController;
  late TextEditingController dobController;
  late TextEditingController addressController;

  var isNewPassword = false.obs;
  var isWorkFromHome = false.obs;
  var obscureText = true.obs;
  var profileUserName = "".obs;
  var isEditRequestProcessing = false.obs;
  var oldpassword_obscureText = true.obs;
  var confirmObscureText = true.obs;
  var isLoading = false.obs;
  var isLoad = false.obs;
  var officeDataFetching = false.obs;
  var shiftDetailsIsLoadinng = false.obs;
  late String deepLink = "".obs as String;
  var profilePicUrl = "".obs;

  @override
  void onInit() {
    super.onInit();
    emailController = TextEditingController();
    passwordController = TextEditingController();
    old_passwordController = TextEditingController();
    confirmPasswordController = TextEditingController();
    forgot_emailController = TextEditingController();

    phoneNumbberController = TextEditingController();
    nameController = TextEditingController();
    designationController = TextEditingController();
    dateOfjoinngController = TextEditingController();
    dobController = TextEditingController();

    addressController = TextEditingController();
    emplyoeeIdController = TextEditingController();
    reportingManagerController = TextEditingController();
    dynamic argumentData = Get.arguments;
    // emailController.text = argumentData[0] ??"";
  }

  @override
  void onClose() {
    super.onClose();
  }

  bool validatePassword(String password) {
    if (password.isNotEmpty) {
      return true;
    } else {
      return false;
    }
  }

  bool validateEmail(String email) {
    if (email.isNotEmpty) {
      return true;
    } else {
      return false;
    }
  }

  void passwordToggle() {
    if (obscureText.value) {
      obscureText(false);
    } else {
      obscureText(true);
    }
  }

  void old_passwordToggle() {
    if (oldpassword_obscureText.value) {
      oldpassword_obscureText(false);
    } else {
      oldpassword_obscureText(true);
    }
  }

  void confirmPasswordToggle() {
    if (confirmObscureText.value) {
      confirmObscureText(false);
    } else {
      confirmObscureText(true);
    }
  }
}