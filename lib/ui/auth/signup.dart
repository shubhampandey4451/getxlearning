import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getxlearning/ui/auth/login.dart';
import 'controller/auth_controller.dart';

class SignUpPage extends StatefulWidget {
  const SignUpPage({Key? key}) : super(key: key);

  @override
  State<SignUpPage> createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  final AuthController _signInController = Get.put(AuthController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        alignment: Alignment.center,
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [

            const Align(
              alignment: Alignment.topLeft,
              child: Text("Register",style: TextStyle(
                  fontSize: 50, fontWeight: FontWeight.bold
              ),),
            ),
            const SizedBox(height: 100,),
            Container(
              padding: const EdgeInsets.fromLTRB(
                  20, 0, 20, 0),
              height: 50,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  color: Colors.black12,
                  borderRadius: BorderRadius.circular(15)),
              child: TextFormField(
                style: const TextStyle(fontSize: 20),
                //readOnly: true,
                // enabled: false,
                controller: _signInController.nameController,
                decoration: const InputDecoration(
                    border: InputBorder.none,
                    hintText: "Full Name",
                    suffixIcon: Icon(Icons.person,
                      color: Colors.grey,size: 25,)
                ),
              ),
            ),
            const SizedBox(height: 20,),
            Container(
              padding: const EdgeInsets.fromLTRB(
                  20, 0, 20, 0),
              height: 50,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  color: Colors.black12,
                  borderRadius: BorderRadius.circular(15)),
              child: TextFormField(
                style: const TextStyle(fontSize: 20),
                //readOnly: true,
                // enabled: false,
                controller: _signInController.phoneNumbberController,
                decoration: const InputDecoration(
                    border: InputBorder.none,
                    hintText: "Phone No.",
                    suffixIcon: Icon(Icons.phone, color: Colors.grey,size: 25,)
                ),
              ),
            ),
            const SizedBox(height: 20,),
            Container(
              padding: const EdgeInsets.fromLTRB(
                  20, 0, 20, 0),
              height: 50,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  color: Colors.black12,
                  borderRadius: BorderRadius.circular(15)),
              child: TextFormField(
                style: const TextStyle(fontSize: 20),
                //readOnly: true,
                // enabled: false,
                controller: _signInController.emailController,
                decoration: const InputDecoration(
                    border: InputBorder.none,
                    hintText: "Email",
                    suffixIcon: Icon(Icons.email, color: Colors.grey,size: 25,)
                ),
              ),
            ),
            const SizedBox(height: 20,),
            Obx(() {
              return Container(
                padding: const EdgeInsets.fromLTRB(
                    20, 0, 20, 0),
                height: 50,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                    color: Colors.black12,
                    borderRadius: BorderRadius.circular(15)),
                child: TextFormField(
                  controller: _signInController.passwordController,
                  obscureText: _signInController.obscureText.value,
                  style: const TextStyle(fontSize: 20),
                  decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: "Password",
                      suffixIcon: InkWell(
                          onTap: () {
                            _signInController.passwordToggle();
                          },
                          child:
                          _signInController.obscureText.value ==
                              false
                              ?  const Icon(
                            Icons.remove_red_eye,
                            color: Colors.grey,
                            size: 25,
                          )
                              :  const Icon(
                              Icons.visibility_off_rounded,
                              color: Colors.grey,
                              size: 25))),
                ),
              );
            }),
            const SizedBox(height: 20,),
            Obx(() {
              return Container(
                padding: const EdgeInsets.fromLTRB(
                    20, 0, 20, 0),
                height: 50,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                    color: Colors.black12,
                    borderRadius: BorderRadius.circular(15)),
                child: TextFormField(
                  controller: _signInController.confirmPasswordController,
                  obscureText: _signInController.confirmObscureText.value,
                  style: const TextStyle(fontSize: 20),
                  decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: "Confirm Password",
                      suffixIcon: InkWell(
                          onTap: () {
                            _signInController.confirmPasswordToggle();
                          },
                          child:
                          _signInController.confirmObscureText.value ==
                              false
                              ?  const Icon(
                            Icons.remove_red_eye,
                            color: Colors.grey,
                            size: 25,
                          )
                              :  const Icon(
                              Icons.visibility_off_rounded, color: Colors.grey,
                              size: 25))),
                ),
              );
            }),
            const SizedBox(height: 30,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  height: 50,
                  width: 120,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(50),
                      color: Colors.grey.shade200),

                  child: const Text(
                    "Sign Up",
                    style:  TextStyle(fontSize: 20),
                  ),),
                InkWell(
                  onTap: () {
                    if (_signInController.validatePassword(
                        _signInController.passwordController.text)
                        && _signInController.validateEmail(_signInController.emailController.text)) {
                      Get.offAll(const LoginPage());
                    } else {
                     // showInSnackBar();
                    }
                  },
                  child: Container(
                    height: 50,
                    width: 120,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50),
                        color: Colors.blue),
                    child: const Icon(
                      Icons.arrow_forward_outlined,
                      size: 30,
                      color: Colors.white,
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(height: 30,),
            Text.rich(
              TextSpan(
                children: [
                  const TextSpan(text: 'Already have account? ',
                      style: TextStyle(fontSize: 16,
                          fontWeight: FontWeight.w500)),
                  TextSpan(
                    text: 'Log In',
                    recognizer:  TapGestureRecognizer()
                      ..onTap = () => {
                        Get.to(() => const LoginPage())
                      },
                    style: const TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w500,
                        color: Colors.blue),
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 30,
            ),
          ],
        ),
      ),
    );
  }
}

