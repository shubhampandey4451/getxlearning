import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getxlearning/ui/auth/signup.dart';
import 'package:getxlearning/ui/movies/home_page.dart';

import '../../utils/CustomColor.dart';
import 'controller/auth_controller.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final AuthController _signInController = Get.put(AuthController());
  final List<TextEditingController> _textEditController = [];
  final GlobalKey<FormState> _key = GlobalKey();
  final List<FocusNode> _focusNode = [];
  FocusNode? pin2FocusNode;
  FocusNode? pin3FocusNode;
  FocusNode? pin4FocusNode;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        alignment: Alignment.center,
        padding: EdgeInsets.symmetric(horizontal: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [

            const Align(
              alignment: Alignment.topLeft,
              child: Text("Login",style: TextStyle(
                  fontSize: 50, fontWeight: FontWeight.bold
              ),),
            ),
            SizedBox(height: 100,),
            Container(
              padding: const EdgeInsets.fromLTRB(
                  20, 0, 20, 0),
              height: 50,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  color: Colors.black12,
                  borderRadius: BorderRadius.circular(15)),
              child: TextFormField(
                style: TextStyle(fontSize: 20),
                //readOnly: true,
                // enabled: false,
                controller: _signInController.emailController,
                decoration: const InputDecoration(
                    border: InputBorder.none,
                    hintText: "Email",
                    suffixIcon: Icon(Icons.email, color: Colors.grey,size: 25,)
                ),
              ),
            ),
            const SizedBox(height: 20,),
            Obx(() {
              return Container(
                padding: const EdgeInsets.fromLTRB(
                    20, 0, 20, 0),
                height: 50,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                    color: Colors.black12,
                    borderRadius: BorderRadius.circular(15)),
                child: TextFormField(
                  controller: _signInController.passwordController,
                  obscureText: _signInController.obscureText.value,
                  style: TextStyle(fontSize: 20),
                  decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: "Password",
                      suffixIcon: InkWell(
                          onTap: () {
                            _signInController.passwordToggle();
                          },
                          child:
                          _signInController.obscureText.value ==
                              false
                              ?  const Icon(
                            Icons.remove_red_eye,
                            color: Colors.grey,
                            size: 25,
                          )
                              :  const Icon(
                              Icons.visibility_off_rounded,
                              color: Colors.grey,
                              size: 25))),
                ),
              );
            }),
            const SizedBox(height: 10,),
            Align(
              alignment: Alignment.bottomRight,
              child: InkWell(
                onTap: () {
                  _signInController.isNewPassword.value = false;
                  bottomSheet(otp: "");
                },
                child: Text(
                  "Forgot Password?",
                  textAlign: TextAlign.end,
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w500,
                      color: Colors.blue),
                ),
              ),
            ),
            Obx(() {
              return Container(
                child: _signInController.isLoading.value
                    ? Center(
                  child: Image.asset(
                    "assets/circle_loder.gif",
                    height: 125.0,
                    width: 125.0,
                  ),
                )
                    : null,
              );
            }),
            SizedBox(height: 20,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  height: 50,
                  width: 120,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(50),
                      color: Colors.grey.shade200),

                child: Text(
                  "Sign In",
                  style:  TextStyle(fontSize: 20),
                ),),
                InkWell(
                  onTap: () {
                    if (_signInController.validatePassword(
                        _signInController.passwordController.text)
                        && _signInController.validateEmail(_signInController.emailController.text)) {
                         Get.offAll(HomePage());
                    } else {
                      showInSnackBar();
                    }
                  },
                  child: Container(
                    height: 50,
                    width: 120,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50),
                        color: Colors.blue),
                    child: const Icon(
                      Icons.arrow_forward_outlined,
                      size: 30,
                      color: Colors.white,
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(height: 30,),
             Text.rich(
              TextSpan(
                children: [
                  const TextSpan(text: 'Did not have account? ',
                  style: TextStyle(fontSize: 16,
                      fontWeight: FontWeight.w500)),
                  TextSpan(
                    text: 'Sign Up',
                    recognizer:  TapGestureRecognizer()
                      ..onTap = () => {
                      Get.to(() => const SignUpPage())
                    },
                    style: const TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w500,
                        color: Colors.blue),
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 30,
            ),
          ],
        ),
      ),
    );
  }

  void bottomSheet({required String otp}) {


    Get.bottomSheet(
      Obx(() {
        _signInController.forgot_emailController.text =    _signInController.emailController.text;
        return Container(
            height: 500,
            padding:
            const EdgeInsets.fromLTRB(30, 0, 30, 0),
            decoration: const BoxDecoration(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20), topRight: Radius.circular(20)),
              color: Colors.white,
            ),
            child: ListView(
              shrinkWrap: true,
              children: [
                Column(
                  children: [
                    const SizedBox(
                      height: 10,
                    ),
                    const SizedBox(
                      width: 100,
                      height: 10,
                      child: Divider(
                        thickness: 3,
                        color: Colors.grey,
                      ),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    Container(
                      child: _signInController.isNewPassword.value == false
                          ? Text(
                        "Forgot Password?",
                        style: const TextStyle(fontSize: 30),
                      )
                          : Text(
                        "New Password? ",
                        style:
                            const TextStyle(fontSize: 30),
                      ),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    Container(
                      child: _signInController.isNewPassword.value == false
                          ? Text(
                        "Enter your registered email verification. We will send a 6-Digit code.",
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontSize: 17),
                      )
                          : Text(
                        "Set your new password to login your auth and access the features",
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 17),
                      ),
                    ),
                    const SizedBox(
                      height: 30,
                    ),
                    Container(
                        child: _signInController.isNewPassword.value == false
                            ? Container(
                          padding: const EdgeInsets.fromLTRB(
                              20, 0, 20, 0),
                          height: 50,
                          decoration: BoxDecoration(
                              color: Colors.black12,
                              borderRadius:
                              BorderRadius.circular(15)),
                          child: TextFormField(
                            style: TextStyle(fontSize: 20),
                            controller:
                            _signInController.forgot_emailController,
                            decoration:  InputDecoration(
                                border: InputBorder.none,
                                hintText: "Enter your registered e-mail",
                                suffixIcon: Icon(
                                  Icons.email,
                                  color: Colors.white,
                                )),
                          ),
                        )
                            : Column(
                          children: [
                            Container(
                              padding: const EdgeInsets.fromLTRB(
                                  20,
                                  0,
                                  20,
                                  0),
                              height: 50,
                              decoration: BoxDecoration(
                                  color: Colors.black12,
                                  borderRadius: BorderRadius.circular(
                                      15)),
                              child: TextFormField(
                                style: TextStyle(fontSize: 20),
                                obscureText:
                                _signInController.obscureText.value,
                                decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText: "Password",
                                    suffixIcon: InkWell(
                                        onTap: () {
                                          _signInController
                                              .passwordToggle();
                                        },
                                        child: _signInController
                                            .obscureText.value ==
                                            false
                                            ?  Icon(
                                          Icons.remove_red_eye,
                                          color: Colors.white,
                                          size: 20,
                                        )
                                            :  Icon(
                                            Icons
                                                .visibility_off_rounded,
                                            color: Colors.white,
                                            size: 20))),
                                controller:
                                _signInController.passwordController,
                              ),
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                            Container(
                              padding: const EdgeInsets.fromLTRB(
                                  20,
                                  0,
                                  20,
                                  0),
                              height: 50,
                              decoration: BoxDecoration(
                                  color: Colors.black12,
                                  borderRadius: BorderRadius.circular(
                                      15)),
                              child: TextFormField(
                                style: TextStyle(fontSize: 20),
                                obscureText: _signInController
                                    .confirmObscureText.value,
                                decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText:
                                    "Confirm Password",
                                    suffixIcon: InkWell(
                                        onTap: () {
                                          _signInController
                                              .confirmPasswordToggle();
                                        },
                                        child: _signInController
                                            .confirmObscureText
                                            .value ==
                                            false
                                            ?  Icon(
                                          Icons.remove_red_eye,
                                          color: Colors.white,
                                          size: 20,
                                        )
                                            :  Icon(
                                            Icons
                                                .visibility_off_rounded,
                                            color: Colors.white,
                                            size: 20))),
                                controller: _signInController
                                    .confirmPasswordController,
                                validator: (value) => value!.length < 8
                                    ? "password can't be less then 8 character"
                                    : null,
                              ),
                            ),
                          ],
                        )),
                      Container(
                      child: _signInController.isNewPassword.value == false
                          ? const SizedBox(
                        height: 100,
                      )
                          : const SizedBox(
                        height: 70,
                      ),
                    ),
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                        Container(
                        height: 50,
                        width: 120,
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(50),
                            color: Colors.grey.shade200),

                        child:
                          Text(
                            "Continue",
                            style: TextStyle(fontSize: 20),
                          ) ),
                          InkWell(
                            onTap: () {
                              if(_signInController.isNewPassword.value == false){
                            if(_signInController.validateEmail(_signInController.forgot_emailController.text)){
                              Get.back();
                              otpVerifyBottomSheet(context);
                            } else{
                              Get.snackbar("Alert!!", "Please enter the register email",
                                  colorText: Colors.white,
                                  backgroundColor: Colors.red,snackPosition: SnackPosition.BOTTOM);
                            }

                            } else{
                                if(_signInController.validatePassword(_signInController.passwordController.text) &&
                                    _signInController.validatePassword(_signInController.confirmPasswordController.text)){
                                  Get.back();
                                } else{
                                  Get.snackbar("Alert!!", "Please enter the password and confirm password",
                                      colorText: Colors.white,
                                      backgroundColor: Colors.red,snackPosition: SnackPosition.BOTTOM);
                                }
                              }
                             },
                            child: _signInController.isLoading.value
                                ? const CircularProgressIndicator()
                                : Container(
                              height: 50,
                              width: 120,
                              decoration: BoxDecoration(
                                  borderRadius:
                                  BorderRadius.circular(50),
                                  color: Colors.blue),
                              child: const Icon(
                                Icons.arrow_forward_outlined,
                                size: 30,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                )
              ],
            ));
      }),
      isDismissible: true,
    );
  }

  void otpVerifyBottomSheet(BuildContext context) {
    _textEditController.clear();
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20), topRight: Radius.circular(20)),
        ),
        builder: (BuildContext context) {
          return Padding(
              padding: EdgeInsets.only(
                  bottom: MediaQuery.of(context).viewInsets.bottom),
              child: Container(
                  padding: const EdgeInsets.fromLTRB(
                      30, 0, 30, 0),
                  decoration: const BoxDecoration(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20),
                        topRight: Radius.circular(20)),
                    color: Colors.white,
                  ),
                  child: ListView(
                    shrinkWrap: true,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Container(
                            width: MediaQuery.of(context).size.width / 2 - 100,
                            padding: const EdgeInsets.all(8.0),
                            margin: const EdgeInsets.all(8.0),
                            height: 4,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(25),
                                color: Colors.grey),
                          ),
                          const SizedBox(
                            height: 30,
                          ),
                          const Text(
                            "Enter 6 Digit Code",
                            textAlign: TextAlign.start,
                            style: TextStyle(
                                fontSize: 32,
                                fontWeight: FontWeight.w400,
                                color: Colors.black),
                          ),
                          const SizedBox(
                            height: 20,
                          ),
                          const Text(
                            "Enter the 6-Digit code that has been sent to your email.",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 18,
                                color: Colors.black,
                                fontWeight: FontWeight.w400,
                                letterSpacing: 0.5),
                          ),
                          const SizedBox(
                            height: 50,
                          ),
                          Form(
                            key: _key,
                            child: Row(
                                mainAxisAlignment:
                                MainAxisAlignment.spaceEvenly,
                                children: List.generate(6, (index) {
                                  _textEditController
                                      .add(TextEditingController());
                                  _focusNode.add(FocusNode());
                                  return SizedBox(
                                    width: 45,
                                    height: 55,
                                    child: TextFormField(
                                      obscureText: true,
                                      controller: _textEditController[index],
                                      focusNode: _focusNode[index],
                                      style: const TextStyle(
                                          fontSize: 24,
                                          color: Colors.black,
                                          fontWeight: FontWeight.bold),
                                      keyboardType: TextInputType.number,
                                      textAlign: TextAlign.center,
                                      decoration: InputDecoration(
                                        contentPadding:
                                        const EdgeInsets.symmetric(
                                            vertical: 15),
                                        border: OutlineInputBorder(
                                          borderRadius:
                                          BorderRadius.circular(15),
                                          borderSide: const BorderSide(
                                              color: Colors.black),
                                        ),
                                        focusedBorder: OutlineInputBorder(
                                          borderRadius:
                                          BorderRadius.circular(15),
                                          borderSide: const BorderSide(
                                              color: Colors.black54, width: 1),
                                        ),
                                        enabledBorder: OutlineInputBorder(
                                          borderRadius:
                                          BorderRadius.circular(15),
                                          borderSide: const BorderSide(
                                              color: Colors.black54, width: 1),
                                        ),
                                      ),
                                      onChanged: (value) {
                                        print("index $index");
                                        if (index == 5) {
                                          _focusNode[index].unfocus();
                                        } else {
                                          nextField(
                                              value, _focusNode[index + 1]);
                                        }
                                      },
                                    ),
                                  );
                                }).toList()),
                          ),
                          const SizedBox(height: 30),
                          Align(
                            alignment: Alignment.center,
                            child: GestureDetector(
                              onTap: () {},
                              child: const Text(
                                "Resend OTP Code",
                                style: TextStyle(
                                    decoration: TextDecoration.underline,
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                          const SizedBox(height: 60),
                          Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                              Container(
                              height: 50,
                              width: 120,
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(50),
                                  color: Colors.grey.shade200),

                              child: Text(
                                  "Continue",
                                  style:  const TextStyle(
                                          fontSize: 20))),
                                InkWell(
                                  onTap: () async {
                                    if (_key.currentState!.validate()) {
                                      _key.currentState!.save();

                                      var otp = StringBuffer();
                                      _textEditController.forEach((element) {
                                        otp.write(element.text.toString());
                                      });
                                      print("otp ${otp.toString()}");
                                      if (otp.length == 6) {
                                          _signInController.isNewPassword(true);
                                          Get.back();
                                          bottomSheet(otp: otp.toString());
                                          _signInController
                                              .passwordController.text = "";
                                          _signInController
                                              .old_passwordController.text = "";
                                          _signInController
                                              .confirmPasswordController
                                              .text = "";
                                        //
                                      }
                                    }
                                  },
                                  child: _signInController.isLoading.value
                                      ? const CircularProgressIndicator()
                                      : Container(
                                    height: 50,
                                    width: 120,
                                    decoration: BoxDecoration(
                                        borderRadius:
                                        BorderRadius.circular(
                                            50),
                                        color: Colors.blue),
                                    child: const Icon(
                                      Icons.arrow_forward_outlined,
                                      size: 30,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ],
                  )));
        });
  }
  void nextField(String value, FocusNode? focusNode) {
    if (value.length == 1) {
      focusNode!.requestFocus();
    }
  }

  void showInSnackBar() {
    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
        dismissDirection: DismissDirection.startToEnd,
        backgroundColor: Colors.red,
        content: Text("Email and Password field can't be blank.")));
  }
}
